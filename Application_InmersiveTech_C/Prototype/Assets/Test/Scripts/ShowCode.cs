using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowCode : MonoBehaviour
{
    public GameObject panelCode;

    // Start is called before the first frame update
    void Start()
    {
        panelCode.SetActive(false);
    }

    void OnTriggerEnter(Collider player)
    {
        if(player.gameObject.tag == "Player")
        {
            panelCode.SetActive(true);
        }
    }
}
