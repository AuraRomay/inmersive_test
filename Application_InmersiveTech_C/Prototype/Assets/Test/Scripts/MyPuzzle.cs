using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyPuzzle : MonoBehaviour
{
	public GameObject door1;

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

	}

	void OnTriggerEnter(Collider player)
	{
		if (player.gameObject.tag == "Player")
		{
			door1.SetActive(false);
		}
	}
}
