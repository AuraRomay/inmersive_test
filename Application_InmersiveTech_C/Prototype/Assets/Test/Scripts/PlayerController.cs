﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public GameObject Cam;
	private CharacterController cc;
	public float speed = 2f;
	public float sensivity = 2f;

	float moveWS;
	float moveAD;

	float rotX;
	float rotY;

	// Use this for initialization
	void Start () {

		cc = GetComponent<CharacterController>();
		
	}
	
	// Update is called once per frame
	void Update()
	{
		moveAD = Input.GetAxis("Horizontal") * speed;
		moveWS = Input.GetAxis("Vertical") * speed;

		rotX = Input.GetAxis("Mouse X") * sensivity;
		rotY = Input.GetAxis("Mouse Y") * sensivity;

		Vector3 movement = new Vector3 (moveAD, 0, moveWS);
		transform.Rotate (0, rotX, 0);
		Cam.transform.Rotate (-rotY, 0, 0);

		movement = transform.rotation * movement;
		cc.Move (movement * Time.deltaTime);
	}
}
