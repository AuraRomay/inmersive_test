using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Display : MonoBehaviour
{
    public GameObject door;
    public GameObject panel;

    [SerializeField]
    private Sprite[] digits;

    [SerializeField]
    private Image[] characters;

    private string codeSequence;

    // Start is called before the first frame update
    void Start()
    {
        codeSequence = "";

        for (int i = 0; i <= characters.Length - 1; i++)
        {
            characters[i].sprite = digits[9];
        }

        ButtonPressed.ButtonPress += AddDigitToCodeSequence;
    }

    // Update is called once per frame
    private void AddDigitToCodeSequence(string digitEntered)
    {
        if (codeSequence.Length < 3)
        {
            switch (digitEntered)
            {
                case "Zero":
                    codeSequence += "0";
                    DisplayCodeSequence(0);
                    break;
                case "One":
                    codeSequence += "1";
                    DisplayCodeSequence(1);
                    break;
                case "Two":
                    codeSequence += "2";
                    DisplayCodeSequence(2);
                    break;
                case "Three":
                    codeSequence += "3";
                    DisplayCodeSequence(3);
                    break;
                case "Four":
                    codeSequence += "4";
                    DisplayCodeSequence(4);
                    break;
                case "Five":
                    codeSequence += "5";
                    DisplayCodeSequence(5);
                    break;
                case "Six":
                    codeSequence += "6";
                    DisplayCodeSequence(6);
                    break;
                case "Seven":
                    codeSequence += "7";
                    DisplayCodeSequence(7);
                    break;
                case "Eight":
                    codeSequence += "8";
                    DisplayCodeSequence(8);
                    break;
                case "Nine":
                    codeSequence += "9";
                    DisplayCodeSequence(9);
                    break;
            }
        }

        switch (digitEntered)
        {
            case "Clear":
                ResetDisplay();
                break;
            case "Enter":
                if (codeSequence.Length > 0)
                {
                    CheckResults();
                }
                break;
        }
    }

    private void DisplayCodeSequence(int digitJustEntered)
    {
        switch (codeSequence.Length)
        {
            case 1:
                characters[0].sprite = digits[9];
                characters[1].sprite = digits[9];
                characters[2].sprite = digits[digitJustEntered];
                break;
            case 2:
                characters[0].sprite = digits[9];
                characters[1].sprite = characters[2].sprite;
                characters[2].sprite = digits[digitJustEntered];
                break;
            case 3:
                characters[0].sprite = characters[1].sprite;
                characters[1].sprite = characters[2].sprite;
                characters[2].sprite = digits[digitJustEntered];
                break;
        }
    }

    private void CheckResults()
    {
        if(codeSequence == "123")
        {
            Debug.Log("Correct");
            door.SetActive(false);
            panel.SetActive(false);
        }
        else
        {
            Debug.Log("Error");
            ResetDisplay();
        }
    }

    private void ResetDisplay()
    {
        for (int i = 0; i <= characters.Length - 1; i++)
        {
            characters[i].sprite = digits[9];
        }

        codeSequence = "";
    }

    private void OnDestroy()
    {
        ButtonPressed.ButtonPress -= AddDigitToCodeSequence;
    }
}
